// AUTHOR
export const CREATE_AUTHOR = "CREATE_AUTHOR";
export const DELETE_AUTHOR = "DELETE_AUTHOR";
export const FETCH_AUTHOR = "FETCH_AUTHOR";
export const FETCH_AUTHORS = "FETCH_AUTHORS";
export const EDIT_AUTHOR = "EDIT_AUTHOR";

// USER
export const CREATE_USER = "CREATE_USER";
export const DELETE_USER = "DELETE_USER";
export const FETCH_USER = "FETCH_USER";
export const FETCH_USERS = "FETCH_USERS";
export const EDIT_USER = "EDIT_USER";

// BOOKS
export const CREATE_BOOK = "CREATE_BOOK";
export const DELETE_BOOK = "DELETE_BOOK";
export const FETCH_BOOK = "FETCH_BOOK";
export const FETCH_BOOKS = "FETCH_BOOKS";
export const EDIT_BOOK = "EDIT_BOOK";

// GENRES
export const FETCH_GENRES = "FETCH_GENRES";
export const FETCH_GENRE = "FETCH_GENRE";
export const CREATE_GENRE = "CREATE_GENRE";

// RENTS
export const CREATE_RENT = "CREATE_RENT";
export const DELETE_RENT = "DELETE_RENT";
export const FETCH_RENT = "FETCH_RENT";
export const FETCH_RENTS = "FETCH_RENTS";
export const EDIT_RENT = "EDIT_RENT";
