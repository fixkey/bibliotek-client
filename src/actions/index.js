import {
  CREATE_AUTHOR,
  DELETE_AUTHOR,
  FETCH_AUTHOR,
  FETCH_AUTHORS,
  EDIT_AUTHOR,
  CREATE_USER,
  DELETE_USER,
  FETCH_USER,
  FETCH_USERS,
  EDIT_USER,
  CREATE_BOOK,
  DELETE_BOOK,
  FETCH_BOOK,
  FETCH_BOOKS,
  EDIT_BOOK,
  FETCH_GENRES,
  FETCH_GENRE,
  CREATE_GENRE,
  CREATE_RENT,
  DELETE_RENT,
  FETCH_RENT,
  FETCH_RENTS,
  EDIT_RENT
} from "./types";
import api from "../apis";

// AUTHOR
export const createAuthor = formData => async dispatch => {
  const response = await api.post("/authors", formData);
  dispatch({ type: CREATE_AUTHOR, payload: response.data });
  alert("Pomyślnie dodano autora"); // TODO
};

export const deleteAuthor = id => async dispatch => {
  await api.delete(`/authors/${id}`);
  dispatch({ type: DELETE_AUTHOR, payload: id });
  alert("Pomyślnie usunięto autora"); // TODO
};

export const fetchAuthor = id => async dispatch => {
  const response = await api.get(`/authors/${id}`);
  dispatch({ type: FETCH_AUTHOR, payload: response.data });
};

export const fetchAuthors = () => async dispatch => {
  const response = await api.get("/authors");
  dispatch({ type: FETCH_AUTHORS, payload: response.data });
};

export const editAuthor = (id, formData) => async dispatch => {
  const response = await api.patch(`/authors/${id}`, formData);
  dispatch({ type: EDIT_AUTHOR, payload: response.data });
  alert("Pomyślnie zedytowano autora"); // TODO
};

// USER
export const createUser = formData => async dispatch => {
  const response = await api.post("/users", formData);
  dispatch({ type: CREATE_USER, payload: response.data });
  alert("Pomyślnie dodano czytelnika"); // TODO
};

export const deleteUser = id => async dispatch => {
  await api.delete(`/users/${id}`);
  dispatch({ type: DELETE_USER, payload: id });
  alert("Pomyślnie usunięto czytelnika"); // TODO
};

export const fetchUser = id => async dispatch => {
  const response = await api.get(`/users/${id}`);
  dispatch({ type: FETCH_USER, payload: response.data });
};

export const fetchUsers = () => async dispatch => {
  const response = await api.get("/users");
  dispatch({ type: FETCH_USERS, payload: response.data });
};

export const editUser = (id, formData) => async dispatch => {
  const response = await api.patch(`/users/${id}`, formData);
  dispatch({ type: EDIT_USER, payload: response.data });
  alert("Pomyślnie zedytowano czytelnika"); // TODO
};

// BOOKS
export const createBook = formData => async dispatch => {
  // (raczej powinno to byc w backendzie)
  // tworzy nowy gatunek jak go jeszcze nie ma
  const genre = formData["genreId"];
  const formattedGenre = `${genre[0].toUpperCase()}${genre
    .slice(1)
    .toLowerCase()}`;
  let genreResponse = await api.get(`/genres?name=${formattedGenre}`);
  if (genreResponse.data.length === 0) {
    await dispatch(createGenre(formattedGenre));
    genreResponse = await api.get(`/genres?name=${formattedGenre}`);
  }
  formData["genreId"] = genreResponse.data[0]["id"];
  const response = await api.post("/books", formData);
  dispatch({ type: CREATE_BOOK, payload: response.data });
  alert("Pomyślnie dodano książkę"); // TODO
};

export const deleteBook = id => async dispatch => {
  // sprawdz czy ksiazka jest wypozyczona
  const response = await api.get(`/rents?bookId=${id}`);

  let bookIsReturned = true;
  // eslint-disable-next-line
  for (let rent of response.data) {
    if (!rent.returnDate) {
      bookIsReturned = false;
    }
  }

  if (bookIsReturned) {
    await api.delete(`/books/${id}`);
    dispatch({ type: DELETE_BOOK, payload: id });
    alert("Pomyślnie usunięto książkę"); // TODO
  } else {
    alert("Książka nie została usunięta, bo jest aktualnie wypożyczona");
  }
};

export const fetchBook = id => async dispatch => {
  const response = await api.get(`/books/${id}`);
  dispatch({ type: FETCH_BOOK, payload: response.data });
};

export const fetchBookWithGenre = id => async dispatch => {
  const responseBook = await api.get(`/books/${id}`);
  const responseGenre = await api.get(`/genres/${responseBook.data.genreId}`);
  dispatch({ type: FETCH_BOOK, payload: responseBook.data });
  dispatch({ type: FETCH_GENRE, payload: responseGenre.data });
};

export const fetchBooks = () => async dispatch => {
  const response = await api.get("/books");
  dispatch({ type: FETCH_BOOKS, payload: response.data });
};

export const editBook = (id, formData) => async dispatch => {
  // (rowniez do backendu)
  // tworzy nowy gatunek jak go jeszcze nie ma
  const genre = formData["genreId"];
  const formattedGenre = `${genre[0].toUpperCase()}${genre
    .slice(1)
    .toLowerCase()}`;
  let genreResponse = await api.get(`/genres?name=${formattedGenre}`);
  if (genreResponse.data.length === 0) {
    await dispatch(createGenre(formattedGenre));
    genreResponse = await api.get(`/genres?name=${formattedGenre}`);
  }
  formData["genreId"] = genreResponse.data[0]["id"];
  const response = await api.patch(`/books/${id}`, formData);
  dispatch({ type: EDIT_BOOK, payload: response.data });
  alert("Pomyślnie zedytowano książkę"); // TODO
};

// GENRES
export const fetchGenres = () => async dispatch => {
  const response = await api.get("/genres");
  dispatch({ type: FETCH_GENRES, payload: response.data });
};

export const createGenre = name => async dispatch => {
  const response = await api.post("/genres", { name });
  dispatch({ type: CREATE_GENRE, payload: response.data });
};

export const fetchGenre = id => async dispatch => {
  const response = await api.get(`/genres/${id}`);
  dispatch({ type: FETCH_GENRE, payload: response.data });
};

// RENTS
export const rentReturnedToday = id => async dispatch => {
  const today = new Date().toISOString().slice(0, 10);
  const response = await api.patch(`/rents/${id}`, { returnDate: today });
  dispatch({ type: EDIT_RENT, payload: response.data });
  alert("Pomyślnie zedytowano wypożyczenie"); // TODO
};

export const createRent = formData => async dispatch => {
  const response = await api.post("/rents", formData);
  dispatch({ type: CREATE_RENT, payload: response.data });
  alert("Pomyślnie dodano wypożyczenie"); // TODO
};

export const deleteRent = id => async dispatch => {
  await api.delete(`/rents/${id}`);
  dispatch({ type: DELETE_RENT, payload: id });
  alert("Pomyślnie usunięto wypożyczenie"); // TODO
};

export const fetchRent = id => async dispatch => {
  const response = await api.get(`/rents/${id}`);
  dispatch({ type: FETCH_RENT, payload: response.data });
};

export const fetchRents = () => async dispatch => {
  const response = await api.get("/rents");
  dispatch({ type: FETCH_RENTS, payload: response.data });
};

export const editRent = (id, formData) => async dispatch => {
  const response = await api.patch(`/rents/${id}`, formData);
  dispatch({ type: EDIT_RENT, payload: response.data });
  alert("Pomyślnie zedytowano wypożyczenie"); // TODO
};
