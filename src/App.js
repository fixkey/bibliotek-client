import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import Dashboard from "./components/pages/Dashboard/Dashboard";
import Books from "./components/pages/Books/Books";
import Authors from "./components/pages/Authors/Authors";
import Users from "./components/pages/Users/Users";
import NotFound from "./components/pages/NotFound";
import Layout from "./components/layout/Layout";
import { CssBaseline } from "@material-ui/core";

import history from "./history";
import Rents from "./components/pages/Rents/Rents";

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <CssBaseline />
        <Layout />
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/rents" component={Rents} />
          <Route path="/books" component={Books} />
          <Route path="/users" component={Users} />
          <Route path="/authors" component={Authors} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
