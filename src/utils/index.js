import { colorsArray } from "./colors";

// zmiana języka tabeli
export const localization = {
  body: {
    emptyDataSourceMessage: "Brak Czytelników do wyświetlenia"
  },
  header: {
    actions: "Akcje"
  },
  toolbar: {
    searchTooltip: "Szukaj",
    searchPlaceholder: "Szukaj"
  },
  pagination: {
    labelRowsSelect: "wierszy",
    labelDisplayedRows: " {from}-{to} z {count}",
    firstTooltip: "Początek",
    previousTooltip: "Poprzednie wiersze",
    nextTooltip: "Dalsze wiersze",
    lastTooltip: "Koniec"
  }
};

// posortuj tablice autorow
export const sortObjectByKey = (person, key) => {
  return person.sort((a, b) => {
    if (a[key] > b[key]) {
      return 1;
    }
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  });
};

// funkcja ktora wypelnia dane w react-chartjs-2 Doughnut
export const calculateGenreData = (books, genres) => {
  if (objectIsEmpty(books) || objectIsEmpty(genres)) {
    return { labels: [""], data: [0], colors: [] };
  }

  // eslint-disable-next-line
  for (let genre of Object.values(genres)) {
    genre.count = 0;
  }

  // eslint-disable-next-line
  for (let book of Object.values(books)) {
    genres[book.genreId].count++;
  }

  let labels = [],
    data = [],
    colors = [];
  // eslint-disable-next-line
  for (let genre of Object.values(genres)) {
    labels.push(genre.name);
    data.push(genre.count);
    colors.push(colorsArray[colors.length]);
  }

  const result = { labels, data, colors };
  return result;
};

export const objectIsEmpty = obj => {
  return Object.getOwnPropertyNames(obj).length === 0;
};

// string puste pole
export const emptyField = "To pole nie może być puste";
