import React from "react";
import { connect } from "react-redux";
import { fetchBooks, fetchGenres } from "../../../actions";
import { Doughnut } from "react-chartjs-2";
import { calculateGenreData } from "../../../utils";

class GenreChart extends React.Component {
  componentDidMount() {
    this.props.fetchBooks();
    this.props.fetchGenres();
  }

  getData = () => {
    const calculatedGenreData = calculateGenreData(
      this.props.books,
      this.props.genres
    );

    return {
      labels: calculatedGenreData.labels,
      datasets: [
        {
          data: calculatedGenreData.data,
          backgroundColor: calculatedGenreData.colors
        }
      ]
    };
  };

  render() {
    return (
      <div className="graph">
        <Doughnut data={this.getData()} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { books: state.books, genres: state.genres };
};

export default connect(
  mapStateToProps,
  {
    fetchBooks,
    fetchGenres
  }
)(GenreChart);
