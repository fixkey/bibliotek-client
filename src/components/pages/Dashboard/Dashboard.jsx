import React, { Component } from "react";
import GenreChart from "./GenreChart";
import RentsTable from "../Rents/RentsTable";
import { Button } from "@material-ui/core";
import history from "../../../history";

class Dashboard extends Component {
  renderDashboard = () => (
    <div>
      <div className="main">
        <GenreChart />
        <div className="rent-button">
          <Button
            onClick={() => history.push("/rents/create")}
            variant="contained"
            color="primary"
          >
            NOWE WYPOŻYCZENIE
          </Button>
        </div>
        <div className="table">
          <RentsTable />
        </div>
      </div>
    </div>
  );

  render() {
    return <div className="container">{this.renderDashboard()}</div>;
  }
}

export default Dashboard;
