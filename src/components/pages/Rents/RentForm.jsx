import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import {
  TextField,
  Button,
  Paper,
  FormControl,
  InputLabel,
  Select,
  FormHelperText
} from "@material-ui/core";

import { connect } from "react-redux";
import { fetchUsers, fetchBooks } from "../../../actions";
import { sortObjectByKey, emptyField } from "../../../utils";

class RentForm extends Component {
  componentDidMount() {
    this.props.fetchBooks();
    this.props.fetchUsers();
  }

  renderTextField = ({ label, input, meta, ...custom }) => (
    <TextField
      label={label}
      placeholder={label}
      error={meta.touched && meta.invalid}
      helperText={meta.touched && meta.error}
      {...input}
      {...custom}
    />
  );

  renderFormHelper = ({ touched, error }) => {
    if (!(touched && error)) {
      return;
    } else {
      return <FormHelperText>{touched && error}</FormHelperText>;
    }
  };

  renderSelectField = ({
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
  }) => (
    <FormControl error={error && touched}>
      <InputLabel>{label}</InputLabel>
      <Select native {...input} {...custom}>
        {children}
      </Select>
      {this.renderFormHelper({ touched, error })}
    </FormControl>
  );

  render() {
    const users = sortObjectByKey(
      Object.values(this.props.users),
      "first_name"
    );

    const books = sortObjectByKey(Object.values(this.props.books), "title");

    return (
      <div className="form-wrap">
        <Paper>
          <form onSubmit={this.props.handleSubmit}>
            <div className="form-group">
              <Field
                name="userId"
                component={this.renderSelectField}
                label="Wypożyczający"
              >
                <option value="" />
                {users.map(user => (
                  <option key={user.id} value={user.id}>{`${
                    user["first_name"]
                  } ${user["last_name"]}(${user["birth_date"]})`}</option>
                ))}
              </Field>
            </div>
            <div className="form-group">
              <Field
                name="bookId"
                component={this.renderSelectField}
                label="Książka"
              >
                <option value="" />
                {books.map(book => (
                  <option key={book.id} value={book.id}>{`${
                    book["title"]
                  } ISBN${book["ISBN"]}`}</option>
                ))}
              </Field>
            </div>
            <div className="form-group">
              <Field
                name="rentalDate"
                component={this.renderTextField}
                label="Data od"
                type="date"
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group">
              <Field
                name="returnDate"
                component={this.renderTextField}
                label="Data do"
                type="date"
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group">
              <Button type="submit" variant="contained" color="primary">
                {this.props.buttonText}
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    );
  }
}

const validate = formValues => {
  const errors = {};
  if (!formValues["userId"]) {
    errors["userId"] = emptyField;
  }

  if (!formValues["bookId"]) {
    errors["bookId"] = emptyField;
  }

  if (!formValues["rentalDate"]) {
    errors["rentalDate"] = emptyField;
  }

  if (formValues["rentalDate"] > formValues["returnDate"]) {
    if (formValues["returnDate"]) {
      errors["rentalDate"] =
        "Data początku nie może być późniejsza niż data końca";
    }
  }

  return errors;
};

const wrappedRentForm = reduxForm({
  form: "rentForm",
  validate
})(RentForm);

const mapStateToProps = state => {
  return { users: state.users, books: state.books };
};

export default connect(
  mapStateToProps,
  {
    fetchUsers,
    fetchBooks
  }
)(wrappedRentForm);
