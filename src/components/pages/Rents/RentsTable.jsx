import React from "react";
import MaterialTable from "material-table";
import { connect } from "react-redux";
import _ from "lodash";
import {
  fetchBooks,
  fetchUsers,
  fetchRents,
  fetchAuthors,
  rentReturnedToday
} from "../../../actions";
import history from "../../../history";
import { localization } from "../../../utils";

class RentsTable extends React.Component {
  componentDidMount() {
    this.props.fetchRents();
    this.props.fetchBooks();
    this.props.fetchAuthors();
    this.props.fetchUsers();
  }

  bookReturnDate = rowData => {
    if (rowData.returnDate) {
      return rowData.returnDate;
    } else {
      return <p className="not-returned">Nie oddano</p>;
    }
  };

  returnBookToday = rowData => {
    this.props.rentReturnedToday(rowData.id);
  };

  render() {
    const usersName = _.mapValues(this.props.users, user => {
      return `${user["first_name"]} ${user["last_name"]}`;
    });

    const usersBday = _.mapValues(this.props.users, user => {
      return `${user["birth_date"]}`;
    });

    const bookTitle = _.mapValues(this.props.books, book => {
      return `${book["title"]}`;
    });

    const bookISBN = _.mapValues(this.props.books, book => {
      return `${book["ISBN"]}`;
    });

    return (
      <MaterialTable
        title="Wypożyczenia"
        columns={[
          { title: "Wypożyczający", field: "userId", lookup: usersName },
          {
            title: "Data urodzenia",
            field: "userId",
            lookup: usersBday
          },
          { title: "Tytuł książki", field: "bookId", lookup: bookTitle },
          { title: "ISBN", field: "bookId", lookup: bookISBN },
          { title: "Data wypożyczenia", field: "rentalDate" },
          {
            title: "Data oddania",
            field: "returnDate",
            render: rowData => this.bookReturnDate(rowData)
          }
        ]}
        data={Object.values(this.props.rents)}
        actions={[
          {
            icon: "create",
            tooltip: "Edytuj wypożyczenie",
            onClick: (event, rowData) =>
              history.push(`/rents/edit/${rowData.id}`)
          },
          {
            icon: "highlight_off",
            tooltip: "Usuń wypożyczenie",
            onClick: (event, rowData) =>
              history.push(`/rents/delete/${rowData.id}`)
          },
          {
            icon: "check_circle",
            tooltip: "Książka oddana dzisiaj",
            onClick: (event, rowData) => this.returnBookToday(rowData)
          }
        ]}
        options={{ sorting: true, actionsColumnIndex: -1 }}
        localization={localization}
      />
    );
  }
}

const mapStateToProps = state => {
  return { rents: state.rents, users: state.users, books: state.books };
};

export default connect(
  mapStateToProps,
  {
    fetchRents,
    fetchUsers,
    fetchBooks,
    fetchAuthors,
    rentReturnedToday
  }
)(RentsTable);
