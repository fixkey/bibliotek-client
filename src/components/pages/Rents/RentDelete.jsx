import React, { Component } from "react";
import { fetchRent, deleteRent } from "../../../actions";
import { connect } from "react-redux";
import history from "../../../history";
import YesNoAlert from "../../YesNoAlert";

class RentDelete extends Component {
  componentDidMount() {
    this.props.fetchRent(this.props.match.params.id);
    this.props.fetchRent(this.props.match.params.id);
  }

  delete = () => {
    this.props.deleteRent(this.props.match.params.id);
  };

  onClose = () => {
    history.push("/");
  };

  renderDialog = () => (
    <YesNoAlert
      onAccept={() => this.delete()}
      onClose={() => this.onClose()}
      dialogTitle="Czy chcesz usunąć wypożyczenie?"
      dialogDescription={`Czy na pewno chcesz usunąć wypożyczenie: ${
        this.props.rent["rentalDate"]
      } ${this.props.rent["returnDate"]}
      ?`}
    />
  );

  render() {
    return <div>{this.props.rent ? this.renderDialog() : "Wczytuję"}</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return { rent: state.rents[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchRent, deleteRent }
)(RentDelete);
