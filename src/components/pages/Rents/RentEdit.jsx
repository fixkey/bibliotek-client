import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchRent, editRent } from "../../../actions";
import RentForm from "./RentForm";

class RentEdit extends Component {
  componentDidMount() {
    this.props.fetchRent(this.props.match.params.id);
  }

  handleSubmit = formProps => {
    this.props.editRent(this.props.match.params.id, formProps);
  };

  render() {
    return (
      <RentForm
        onSubmit={this.handleSubmit}
        initialValues={this.props.rent}
        buttonText="EDYTUJ WYPOZYCZENIE"
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    rent: state.rents[ownProps.match.params.id]
  };
};

export default connect(
  mapStateToProps,
  { fetchRent, editRent }
)(RentEdit);
