import React, { Component } from "react";
import { connect } from "react-redux";
import { createRent } from "../../../actions";
import RentForm from "./RentForm";

class RentCreate extends Component {
  handleSubmit = formProps => {
    this.props.createRent(formProps);
  };

  render() {
    return (
      <RentForm onSubmit={this.handleSubmit} buttonText="DODAJ WYPOŻYCZENIE" />
    );
  }
}

export default connect(
  null,
  { createRent }
)(RentCreate);
