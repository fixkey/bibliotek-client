import React from "react";
import { Route, Router, Switch } from "react-router-dom";
import history from "../../../history";
import RentCreate from "./RentCreate";
import RentEdit from "./RentEdit";
import RentDelete from "./RentDelete";

const Rents = () => {
  return (
    <div className="container">
      <Router history={history}>
        <Switch>
          <Route exact path="/rents/create" component={RentCreate} />
          <Route exact path="/rents/edit/:id" component={RentEdit} />
          <Route exact path="/rents/delete/:id" component={RentDelete} />
        </Switch>
      </Router>
    </div>
  );
};

export default Rents;
