import React, { Component } from "react";
import { fetchBook, deleteBook } from "../../../actions";
import { connect } from "react-redux";
import history from "../../../history";
import YesNoAlert from "../../YesNoAlert";

class BookDelete extends Component {
  componentDidMount() {
    this.props.fetchBook(this.props.match.params.id);
  }

  delete = () => {
    this.props.deleteBook(this.props.match.params.id);
  };

  onClose = () => {
    history.push("/books");
  };

  renderDialog = () => (
    <YesNoAlert
      onAccept={() => this.delete()}
      onClose={() => this.onClose()}
      dialogTitle="Czy chcesz usunąć książkę?"
      dialogDescription={`Czy na pewno chcesz usunąć książkę: ${
        this.props.book["title"]
      }?`}
    />
  );

  render() {
    return <div>{this.props.book ? this.renderDialog() : "Wczytuję"}</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return { book: state.books[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchBook, deleteBook }
)(BookDelete);
