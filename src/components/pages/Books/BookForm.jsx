import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import {
  TextField,
  Button,
  Paper,
  FormControl,
  InputLabel,
  Select,
  FormHelperText
} from "@material-ui/core";

import { connect } from "react-redux";
import { fetchAuthors } from "../../../actions";
import { sortObjectByKey, emptyField } from "../../../utils";

class BookForm extends Component {
  componentDidMount() {
    this.props.fetchAuthors();
  }

  renderTextField = ({ label, input, meta, ...custom }) => (
    <TextField
      label={label}
      placeholder={label}
      error={meta.touched && meta.invalid}
      helperText={meta.touched && meta.error}
      {...input}
      {...custom}
    />
  );

  renderFormHelper = ({ touched, error }) => {
    if (!(touched && error)) {
      return;
    } else {
      return <FormHelperText>{touched && error}</FormHelperText>;
    }
  };

  renderSelectField = ({
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
  }) => (
    <FormControl error={error && touched}>
      <InputLabel>{label}</InputLabel>
      <Select native {...input} {...custom}>
        {children}
      </Select>
      {this.renderFormHelper({ touched, error })}
    </FormControl>
  );

  render() {
    const authors = sortObjectByKey(
      Object.values(this.props.authors),
      "first_name"
    );
    return (
      <div className="form-wrap">
        <Paper>
          <form onSubmit={this.props.handleSubmit}>
            <div className="form-group">
              <Field
                name="title"
                component={this.renderTextField}
                label="Tytuł"
              />
            </div>
            <div className="form-group">
              <Field
                name="authorId"
                component={this.renderSelectField}
                label="Autor"
              >
                <option value="" />
                {authors.map(author => (
                  <option key={author.id} value={author.id}>{`${
                    author["first_name"]
                  } ${author["last_name"]}`}</option>
                ))}
              </Field>
            </div>
            <div className="form-group">
              <Field
                name="releaseDate"
                component={this.renderTextField}
                label="Data wydania"
                type="date"
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group">
              <Field
                name="genreId"
                component={this.renderTextField}
                label="Gatunek"
              />
            </div>
            <div className="form-group">
              <Field
                name="ISBN"
                component={this.renderTextField}
                label="ISBN"
              />
            </div>
            <div className="form-group">
              <Button type="submit" variant="contained" color="primary">
                {this.props.buttonText}
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    );
  }
}

const validate = formValues => {
  const errors = {};
  if (!formValues["title"]) {
    errors["title"] = emptyField;
  }

  if (!formValues["authorId"]) {
    errors["authorId"] = "Wybierz autora";
  }

  if (
    !/^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/.test(
      formValues["ISBN"]
    )
  ) {
    errors["ISBN"] = "Niepoprawny ISBN";
  }

  if (!formValues["releaseDate"]) {
    errors["releaseDate"] = emptyField;
  }

  if (!formValues["genreId"]) {
    errors["genreId"] = emptyField;
  }

  return errors;
};

const wrappedBookForm = reduxForm({
  form: "bookForm",
  validate
})(BookForm);

const mapStateToProps = state => {
  return { authors: state.authors };
};

export default connect(
  mapStateToProps,
  {
    fetchAuthors
  }
)(wrappedBookForm);
