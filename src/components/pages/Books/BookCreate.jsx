import React, { Component } from "react";
import { connect } from "react-redux";
import { createBook } from "../../../actions";
import BookForm from "./BookForm";

class BookCreate extends Component {
  handleSubmit = formProps => {
    this.props.createBook(formProps);
  };

  render() {
    return <BookForm onSubmit={this.handleSubmit} buttonText="DODAJ KSIĄŻKĘ" />;
  }
}

export default connect(
  null,
  { createBook }
)(BookCreate);
