import React from "react";
import MaterialTable from "material-table";
import { connect } from "react-redux";
import _ from "lodash";
import { fetchBooks, fetchAuthors, fetchGenres } from "../../../actions";
import history from "../../../history";
import { localization } from "../../../utils";

class BooksTable extends React.Component {
  componentDidMount() {
    this.props.fetchBooks();
    this.props.fetchAuthors();
    this.props.fetchGenres();
  }

  render() {
    const authors = _.mapValues(this.props.authors, author => {
      return `${author["first_name"]} ${author["last_name"]}`;
    });
    const genres = _.mapValues(this.props.genres, "name");
    return (
      <MaterialTable
        title="Książki"
        columns={[
          { title: "Tytuł", field: "title" },
          {
            title: "Autor",
            field: "authorId",
            lookup: authors
          },
          {
            title: "Data wydania",
            field: "releaseDate",
            customSort: (a, b) =>
              Date.parse(a["releaseDate"]) - Date.parse(b["releaseDate"])
          },
          {
            title: "Gatunek",
            field: "genreId",
            lookup: genres
          }
        ]}
        data={Object.values(this.props.books)}
        actions={[
          {
            icon: "create",
            tooltip: "Edytuj książkę",
            onClick: (event, rowData) =>
              history.push(`/books/edit/${rowData.id}`)
          },
          {
            icon: "highlight_off",
            tooltip: "Usuń książkę",
            onClick: (event, rowData) =>
              history.push(`/books/delete/${rowData.id}`)
          }
        ]}
        options={{ sorting: true, actionsColumnIndex: -1 }}
        localization={localization}
      />
    );
  }
}

const mapStateToProps = state => {
  return { books: state.books, authors: state.authors, genres: state.genres };
};

export default connect(
  mapStateToProps,
  {
    fetchBooks,
    fetchAuthors,
    fetchGenres
  }
)(BooksTable);
