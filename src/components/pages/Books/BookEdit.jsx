import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchBookWithGenre, editBook, fetchGenres } from "../../../actions";
import BookForm from "./BookForm";
import { objectIsEmpty } from "../../../utils";

class BookEdit extends Component {
  componentDidMount() {
    this.props.fetchBookWithGenre(this.props.match.params.id);
    this.props.fetchGenres(); // bez tego refresh -> dashboard konczyl sie katastofa
  }

  handleSubmit = formProps => {
    this.props.editBook(this.props.match.params.id, formProps);
  };

  render() {
    if (!this.props.genre) {
      return <BookForm buttonText="EDYTUJ AUTORA" />;
    } else {
      return (
        <BookForm
          onSubmit={this.handleSubmit}
          initialValues={{
            ...this.props.book,
            genreId: this.props.genre.name
          }}
          buttonText="EDYTUJ AUTORA"
        />
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  if (objectIsEmpty(state.books)) {
    return {};
  }
  return {
    book: state.books[ownProps.match.params.id],
    genre: state.genres[state.books[ownProps.match.params.id].genreId]
  };
};

export default connect(
  mapStateToProps,
  { fetchBookWithGenre, editBook, fetchGenres }
)(BookEdit);
