import React, { Component } from "react";
import BooksTable from "./BooksTable";
import { Button } from "@material-ui/core";
import BookCreate from "./BookCreate";
import BookEdit from "./BookEdit";
import BookDelete from "./BookDelete";

import { Router, Route, Switch } from "react-router-dom";
import history from "../../../history";

class Books extends Component {
  renderTable = () => (
    <div>
      <div className="table">
        <BooksTable />
      </div>
      <div className="align-right">
        <Button
          onClick={() => history.push("/books/create")}
          variant="contained"
          color="primary"
        >
          DODAJ KSIĄŻKĘ
        </Button>
      </div>
    </div>
  );

  render() {
    return (
      <div className="container">
        <Router history={history}>
          <Switch>
            <Route exact path="/books/" component={this.renderTable} />
            <Route exact path="/books/create" component={BookCreate} />
            <Route exact path="/books/edit/:id" component={BookEdit} />
            <Route exact path="/books/delete/:id" component={BookDelete} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Books;
