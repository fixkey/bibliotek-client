import React, { Component } from "react";
import { connect } from "react-redux";
import { createUser } from "../../../actions";
import UserForm from "./UserForm";

class AuthorCreate extends Component {
  handleSubmit = formProps => {
    this.props.createUser(formProps);
  };

  render() {
    return (
      <UserForm onSubmit={this.handleSubmit} buttonText="DODAJ CZYTELNIKA" />
    );
  }
}

export default connect(
  null,
  { createUser }
)(AuthorCreate);
