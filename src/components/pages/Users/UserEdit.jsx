import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchUser, editUser } from "../../../actions";
import UserForm from "./UserForm";

class UserEdit extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.match.params.id);
  }

  handleSubmit = formProps => {
    this.props.editUser(this.props.match.params.id, formProps);
  };

  render() {
    return (
      <UserForm
        onSubmit={this.handleSubmit}
        initialValues={this.props.user}
        buttonText="EDYTUJ CZYTELNIKA"
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { user: state.users[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchUser, editUser }
)(UserEdit);
