import React, { Component } from "react";
import { fetchUser, deleteUser } from "../../../actions";
import { connect } from "react-redux";
import history from "../../../history";
import YesNoAlert from "../../YesNoAlert";

class UserDelete extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.match.params.id);
  }

  delete = () => {
    this.props.deleteUser(this.props.match.params.id);
  };

  onClose = () => {
    history.push("/users");
  };

  renderDialog = () => (
    <YesNoAlert
      onAccept={() => this.delete()}
      onClose={() => this.onClose()}
      dialogTitle="Czy chcesz usunąć czytelnika?"
      dialogDescription={`Czy na pewno chcesz usunąć czytelnika: ${
        this.props.user["first_name"]
      } ${this.props.user["last_name"]}?`}
    />
  );

  render() {
    return <div>{this.props.user ? this.renderDialog() : "Wczytuję"}</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return { user: state.users[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchUser, deleteUser }
)(UserDelete);
