import React from "react";
import MaterialTable from "material-table";
import history from "../../../history";
import { connect } from "react-redux";
import { fetchUsers } from "../../../actions";
import { localization } from "../../../utils";

class UsersTable extends React.Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <MaterialTable
        title="Czytelnicy"
        columns={[
          { title: "Imię", field: "first_name" },
          { title: "Nazwisko", field: "last_name" },
          { title: "Email", field: "email" },
          {
            title: "Data urodzenia",
            field: "birth_date",
            customSort: (a, b) =>
              Date.parse(a["birth_date"]) - Date.parse(b["birth_date"])
          }
        ]}
        data={Object.values(this.props.users)}
        actions={[
          {
            icon: "create",
            tooltip: "Edytuj czytelnika",
            onClick: (event, rowData) =>
              history.push(`/users/edit/${rowData.id}`)
          },
          {
            icon: "highlight_off",
            tooltip: "Usuń czytelnika",
            onClick: (event, rowData) =>
              history.push(`/users/delete/${rowData.id}`)
          }
        ]}
        options={{ sorting: true, actionsColumnIndex: -1 }}
        localization={localization}
      />
    );
  }
}

const mapStateToProps = state => {
  return { users: state.users };
};

export default connect(
  mapStateToProps,
  {
    fetchUsers
  }
)(UsersTable);
