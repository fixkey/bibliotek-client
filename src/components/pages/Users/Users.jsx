import React, { Component } from "react";
import { Button } from "@material-ui/core";
import { Router, Switch, Route } from "react-router-dom";
import history from "../../../history";
import UsersTable from "./UsersTable";
import UserCreate from "./UserCreate";
import UserEdit from "./UserEdit";
import UserDelete from "./UserDelete";

class Users extends Component {
  renderTable = () => (
    <div>
      <div className="table">
        <UsersTable />
      </div>
      <div className="align-right">
        <Button
          onClick={() => history.push("/users/create")}
          variant="contained"
          color="primary"
        >
          DODAJ CZYTELNIKA
        </Button>
      </div>
    </div>
  );

  render() {
    return (
      <div className="container">
        <Router history={history}>
          <Switch>
            <Route exact path="/users/" component={this.renderTable} />
            <Route exact path="/users/create" component={UserCreate} />
            <Route exact path="/users/edit/:id" component={UserEdit} />
            <Route exact path="/users/delete/:id" component={UserDelete} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Users;
