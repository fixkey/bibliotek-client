import React, { Component } from "react";
import { fetchAuthor, deleteAuthor } from "../../../actions";
import { connect } from "react-redux";
import history from "../../../history";
import YesNoAlert from "../../YesNoAlert";

class AuthorDelete extends Component {
  componentDidMount() {
    this.props.fetchAuthor(this.props.match.params.id);
  }

  delete = () => {
    this.props.deleteAuthor(this.props.match.params.id);
  };

  onClose = () => {
    history.push("/authors");
  };

  renderDialog = () => (
    <YesNoAlert
      onAccept={() => this.delete()}
      onClose={() => this.onClose()}
      dialogTitle="Czy chcesz usunąć autora?"
      dialogDescription={`Czy na pewno chcesz usunąć autora: ${
        this.props.author["first_name"]
      } ${this.props.author["last_name"]}?`}
    />
  );

  render() {
    return <div>{this.props.author ? this.renderDialog() : "Wczytuję"}</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return { author: state.authors[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchAuthor, deleteAuthor }
)(AuthorDelete);
