import React, { Component } from "react";

import AuthorsTable from "./AuthorsTable";
import { Button } from "@material-ui/core";

import { Router, Route, Switch } from "react-router-dom";
import history from "../../../history";
import AuthorCreate from "./AuthorCreate";
import AuthorEdit from "./AuthorEdit";
import AuthorDelete from "./AuthorDelete";

class Authors extends Component {
  renderTable = () => (
    <div>
      <div className="table">
        <AuthorsTable />
      </div>
      <div className="align-right">
        <Button
          onClick={() => history.push("/authors/create")}
          variant="contained"
          color="primary"
        >
          DODAJ AUTORA
        </Button>
      </div>
    </div>
  );

  render() {
    return (
      <div className="container">
        <Router history={history}>
          <Switch>
            <Route exact path="/authors/" component={this.renderTable} />
            <Route exact path="/authors/create" component={AuthorCreate} />
            <Route exact path="/authors/edit/:id" component={AuthorEdit} />
            <Route exact path="/authors/delete/:id" component={AuthorDelete} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Authors;
