import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { TextField, Button, Paper } from "@material-ui/core";
import { emptyField } from "../../../utils";

class AuthorForm extends Component {
  renderTextField = ({ label, input, meta, ...custom }) => (
    <TextField
      label={label}
      placeholder={label}
      error={meta.touched && meta.invalid}
      helperText={meta.touched && meta.error}
      {...input}
      {...custom}
    />
  );

  render() {
    return (
      <div className="form-wrap">
        <Paper>
          <form onSubmit={this.props.handleSubmit}>
            <div className="form-group">
              <Field
                name="first_name"
                component={this.renderTextField}
                label="Imię"
              />
            </div>
            <div className="form-group">
              <Field
                name="last_name"
                component={this.renderTextField}
                label="Nazwisko"
              />
            </div>
            <div className="form-group">
              <Button type="submit" variant="contained" color="primary">
                {this.props.buttonText}
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    );
  }
}

const validate = formValues => {
  const errors = {};
  if (!formValues["first_name"]) {
    errors["first_name"] = emptyField;
  }

  if (!formValues["last_name"]) {
    errors["last_name"] = emptyField;
  }

  return errors;
};

export default reduxForm({
  form: "authorForm",
  validate
})(AuthorForm);
