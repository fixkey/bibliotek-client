import React from "react";
import MaterialTable from "material-table";
import history from "../../../history";
import { connect } from "react-redux";
import { fetchAuthors } from "../../../actions";
import { localization } from "../../../utils";
class AuthorsTable extends React.Component {
  componentDidMount() {
    this.props.fetchAuthors();
  }

  render() {
    return (
      <MaterialTable
        title="Autorzy"
        columns={[
          { title: "Imię", field: "first_name" },
          { title: "Nazwisko", field: "last_name" }
        ]}
        data={Object.values(this.props.authors)}
        actions={[
          {
            icon: "create",
            tooltip: "Edytuj autora",
            onClick: (event, rowData) =>
              history.push(`/authors/edit/${rowData.id}`)
          },
          {
            icon: "highlight_off",
            tooltip: "Usuń autora",
            onClick: (event, rowData) =>
              history.push(`/authors/delete/${rowData.id}`)
          }
        ]}
        options={{ sorting: true, actionsColumnIndex: -1 }}
        localization={localization}
      />
    );
  }
}

const mapStateToProps = state => {
  return { authors: state.authors };
};

export default connect(
  mapStateToProps,
  {
    fetchAuthors
  }
)(AuthorsTable);
