import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchAuthor, editAuthor } from "../../../actions";
import AuthorForm from "./AuthorForm";

class AuthorEdit extends Component {
  componentDidMount() {
    this.props.fetchAuthor(this.props.match.params.id);
  }

  handleSubmit = formProps => {
    this.props.editAuthor(this.props.match.params.id, formProps);
  };

  render() {
    return (
      <AuthorForm
        onSubmit={this.handleSubmit}
        initialValues={this.props.author}
        buttonText="EDYTUJ AUTORA"
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { author: state.authors[ownProps.match.params.id] };
};

export default connect(
  mapStateToProps,
  { fetchAuthor, editAuthor }
)(AuthorEdit);
