import React, { Component } from "react";
import { connect } from "react-redux";
import { createAuthor } from "../../../actions";
import AuthorForm from "./AuthorForm";

class AuthorCreate extends Component {
  handleSubmit = formProps => {
    this.props.createAuthor(formProps);
  };

  render() {
    return (
      <AuthorForm onSubmit={this.handleSubmit} buttonText="DODAJ AUTORA" />
    );
  }
}

export default connect(
  null,
  { createAuthor }
)(AuthorCreate);
