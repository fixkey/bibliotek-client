import React from "react";

const NotFound = () => {
  return (
    <div className="container">
      <div className="main">404</div>
    </div>
  );
};

export default NotFound;
