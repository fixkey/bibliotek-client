import React from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <AppBar position="fixed" style={{ boxShadow: "none", height: "60px" }}>
        <Toolbar>
          <Typography variant="h4" className="title">
            <Link to="/" className="logo">
              Bibliotek
            </Link>
          </Typography>
          {/* login nie wspierany */}
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </header>
  );
};

export default Header;
