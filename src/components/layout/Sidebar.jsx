import React from "react";
import { List } from "@material-ui/core";

import SidebarListItem from "./SidebarListItem";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <List component="nav">
        <SidebarListItem text="Dashboard" icon="dashboard" link="/" />
        <SidebarListItem text="Książki" icon="library_books" link="/books" />
        <SidebarListItem text="Czytelnicy" icon="people" link="/users" />
        <SidebarListItem text="Autorzy" icon="face" link="/authors" />
      </List>
    </div>
  );
};

export default Sidebar;
