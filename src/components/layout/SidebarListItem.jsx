import React from "react";

import { ListItem, ListItemIcon, Icon, ListItemText } from "@material-ui/core";

import history from "../../history";

const SidebarListItem = props => {
  return (
    <ListItem
      onClick={() => history.push(props.link)}
      button
      // selected={history.location.pathname === props.link}
    >
      <ListItemIcon>
        <Icon>{props.icon}</Icon>
      </ListItemIcon>
      <ListItemText primary={props.text} />
    </ListItem>
  );
};

export default SidebarListItem;
