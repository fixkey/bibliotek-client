import React, { Component } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button
} from "@material-ui/core";

// dialogTitle, dialogDescription,
class YesNoAlert extends Component {
  // componentDidMount() {
  //   this.props.fetchAction(this.props.match.params.id); // fetchAuthor
  // }

  handleAccept = () => {
    this.props.onAccept();
    this.handleClose();
  };

  handleClose = () => {
    this.props.onClose();
  };

  renderDialog = () => (
    <Dialog
      open={true}
      onClose={this.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        {this.props.dialogTitle}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {this.props.dialogDescription}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={this.handleClose} color="primary">
          Nie
        </Button>
        <Button onClick={this.handleAccept} color="primary">
          Tak
        </Button>
      </DialogActions>
    </Dialog>
  );

  render() {
    // return <div>{this.props.author ? this.renderDialog() : "Wczytuję"}</div>;
    return <div>{this.renderDialog()}</div>;
  }
}

// const mapStateToProps = (state, ownProps) => {
//   return { author: state.authors[ownProps.match.params.id] };
// };

// export default connect(
//   mapStateToProps,
//   { fetchAuthor, deleteAuthor }
// )(AuthorDelete);

export default YesNoAlert;
