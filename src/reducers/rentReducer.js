import _ from "lodash";

import {
  CREATE_RENT,
  DELETE_RENT,
  FETCH_RENT,
  FETCH_RENTS,
  EDIT_RENT
} from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_RENT:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_RENTS:
      return { ...state, ..._.mapKeys(action.payload, "id") }; //chyba obyloby sie bez "...state"
    case FETCH_RENT:
      return { ...state, [action.payload.id]: action.payload };
    case EDIT_RENT:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_RENT: {
      return { ..._.omit(state, action.payload) };
    }
    default:
      return state;
  }
};
