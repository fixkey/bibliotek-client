import _ from "lodash";

import {
  CREATE_AUTHOR,
  DELETE_AUTHOR,
  FETCH_AUTHOR,
  FETCH_AUTHORS,
  EDIT_AUTHOR
} from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_AUTHOR:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_AUTHORS:
      return { ...state, ..._.mapKeys(action.payload, "id") }; //chyba obyloby sie bez "...state"
    case FETCH_AUTHOR:
      return { ...state, [action.payload.id]: action.payload };
    case EDIT_AUTHOR:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_AUTHOR: {
      return { ..._.omit(state, action.payload) };
    }
    default:
      return state;
  }
};
