import _ from "lodash";

import { FETCH_GENRES, FETCH_GENRE } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_GENRES:
      return { ...state, ..._.mapKeys(action.payload, "id") }; //chyba obyloby sie bez "...state"
    case FETCH_GENRE:
      return { ...state, [action.payload.id]: action.payload };
    default:
      return state;
  }
};
