import { combineReducers } from "redux";
import authorReducer from "./authorReducer";
import { reducer as formReducer } from "redux-form";
import userReducer from "./userReducer";
import bookReducer from "./bookReducer";
import genreReducer from "./genreReducer";
import rentReducer from "./rentReducer";

export default combineReducers({
  authors: authorReducer,
  users: userReducer,
  books: bookReducer,
  genres: genreReducer,
  rents: rentReducer,
  form: formReducer
});
